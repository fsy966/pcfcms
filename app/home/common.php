<?php

if (!function_exists('set_home_url_mode')) 
{
    // 设置前台URL模式
    function set_home_url_mode() {
        $seo_pseudo = tpCache('seo.seo_pseudo');
        if ($seo_pseudo == 1) {
            config('route.url_common_param', true);
            config('route.url_route_must', false);
        } elseif ($seo_pseudo == 2) {
            config('route.url_common_param', false);
            config('route.url_route_must', true);
        } elseif ($seo_pseudo == 3) {
            config('route.url_common_param', false);
            config('route.url_route_must', true);
        }
    }
}

if (!function_exists('set_arcseotitle')) 
{
    // 设置内容标题
    function set_arcseotitle($title = '', $seo_title = '', $typename = '')
    {
        // 针对没有自定义SEO标题的文档
        if (empty($seo_title)) {
            static $web_name = null;
            null === $web_name && $web_name = tpCache('web.web_name');
            static $seo_viewtitle_format = null;
            null === $seo_viewtitle_format && $seo_viewtitle_format = tpCache('seo.seo_viewtitle_format');
            //查看授权状态
            $is_gzpcf_authortoken = !empty(tpCache('web.web_is_authortoken')) ? tpCache('web.web_is_authortoken') : 0;
            if($is_gzpcf_authortoken < 0){
                switch ($seo_viewtitle_format) 
                {
                    case '1':
                        $seo_title = $title.'_'.'by pcfcms';
                        break;
                    case '3':
                        $seo_title = $title.'_'.$typename.'_'.$web_name.'_'.'by pcfcms';
                        break;
                    case '2':
                    default:
                        $seo_title = $title.'_'.$web_name.'_'.'by pcfcms';
                        break;
                } 
            }else{
                switch ($seo_viewtitle_format) 
                {
                    case '1':
                        $seo_title = $title;
                        break;
                    case '3':
                        $seo_title = $title.'_'.$typename.'_'.$web_name;
                        break;
                    case '2':
                    default:
                        $seo_title = $title.'_'.$web_name;
                        break;
                }             
            }
        }
        return $seo_title;
    }
}

if (!function_exists('set_typeseotitle')) 
{
    // 设置栏目标题
    function set_typeseotitle($typename = '', $seo_title = '')
    {
        // 针对没有自定义SEO标题的列表
        if (empty($seo_title)) {
            $web_name = tpCache('web.web_name');
            $seo_liststitle_format = tpCache('seo.seo_liststitle_format');
            //查看授权状态
            $is_gzpcf_authortoken = !empty(tpCache('web.web_is_authortoken')) ? tpCache('web.web_is_authortoken') : 0;
            if($is_gzpcf_authortoken < 0){
                switch ($seo_liststitle_format) 
                {
                    case '1':
                        $seo_title = $typename.'_'.$web_name.'_'.'by pcfcms';
                        break;
                    case '2':
                    default:
                        $page = input('param.page/d', 1);
                        if ($page > 1) {
                            $typename .= "_第{$page}页";
                        }
                        $seo_title = $typename.'_'.$web_name.'_'.'by pcfcms';
                        break;
                }
            }else{
                switch ($seo_liststitle_format) 
                {
                    case '1':
                        $seo_title = $typename.'_'.$web_name;
                        break;
                    case '2':
                    default:
                        $page = input('param.page/d', 1);
                        if ($page > 1) {
                            $typename .= "_第{$page}页";
                        }
                        $seo_title = $typename.'_'.$web_name;
                        break;
                }    
            }
        }
        return $seo_title;
    }
}

if (!function_exists('MyDate')) 
{
    /**
     *  时间转化日期格式
     * @param     string  $format     日期格式
     * @param     intval  $t     时间戳
     * @return    string
     */
    function MyDate($format = 'Y-m-d', $t = '')
    {
        if (!empty($t)) {
            $t = date($format, $t);
        }
        return $t;
    }
}

if (!function_exists('home_time_ago')) 
{
	function home_time_ago($format = 'Y-m-d', $posttime = '')
	{
		//当前时间的时间戳
		$nowtimes = time();
		//相差时间戳
		$counttime = $nowtimes - $posttime;
		//进行时间转换
		if ($counttime <= 60) {
			return '刚刚';
		} else if ($counttime > 60 && $counttime <= 120) {
			return '1分钟前';
		} else if ($counttime > 120 && $counttime <= 180) {
			return '2分钟前';
		} else if ($counttime > 180 && $counttime < 3600) {
			return intval(($counttime / 60)) . '分钟前';
		} else if ($counttime >= 3600 && $counttime < 3600 * 24) {
			return intval(($counttime / 3600)) . '小时前';
		} else if ($counttime >= 3600 * 24 && $counttime < 3600 * 24 * 2) {
			return '昨天';
		} else if ($counttime >= 3600 * 24 * 2 && $counttime < 3600 * 24 * 3) {
			return '前天';
		} else if ($counttime >= 3600 * 24 * 3 && $counttime <= 3600 * 24 * 7) {
			return intval(($counttime / (3600 * 24))) . '天前';
		} else if ($counttime >= 3600 * 24 * 7 && $counttime <= 3600 * 24 * 30) {
			return intval(($counttime / (3600 * 24 * 7))) . '周前';
		} else if ($counttime >= 3600 * 24 * 30 && $counttime <= 3600 * 24 * 365) {
			return intval(($counttime / (3600 * 24 * 30))) . '个月前';
		} else if ($counttime >= 3600 * 24 * 365) {
			return intval(($counttime / (3600 * 24 * 365))) . '年前';
		}
	}
}

if (!function_exists('html_msubstr')) 
{
    /**
     * 截取内容清除html之后的字符串长度，支持中文和其他编码
     * @param string $str 需要转换的字符串
     * @param string $start 开始位置
     * @param string $length 截取长度
     * @param string $suffix 截断显示字符
     * @param string $charset 编码格式
     * @return string
     */
    function html_msubstr($str='', $start=0, $length=NULL, $suffix=false, $charset="utf-8") {
        $str = gzpcf_htmlspecialchars_decode($str);
        $str = checkStrHtml($str);
        return pcfmsubstr($str, $start, $length, $suffix, $charset);
    }
}

if (!function_exists('gzpcf_htmlspecialchars_decode')) 
{
    /**
     * 自定义只针对htmlspecialchars编码过的字符串进行解码
     *
     * @param string $str 需要转换的字符串
     * @param string $start 开始位置
     * @param string $length 截取长度
     * @param string $suffix 截断显示字符
     * @param string $charset 编码格式
     * @return string
     */
    function gzpcf_htmlspecialchars_decode($str='') {
        if (is_string($str) && stripos($str, '&lt;') !== false && stripos($str, '&gt;') !== false) {
            $str = htmlspecialchars_decode($str);
        }
        return $str;
    }
}



