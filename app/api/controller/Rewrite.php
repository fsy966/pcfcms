<?php
/**
 * 伪静态
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */
namespace app\api\controller;

class Rewrite extends Base
{
    // 初始化操作
    public function initialize() {
        parent::initialize();
    }

    // 检测服务器是否支持URL重写隐藏应用的入口文件index.php
    public function testing()
    {
        ob_clean();
    }

    // 设置隐藏index.php
    public function setInlet()
    {
        $seo_inlet = input('param.seo_inlet/d', 1);
        tpCache('seo', ['seo_inlet'=>$seo_inlet]);
        ob_clean();
    }
}