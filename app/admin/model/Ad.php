<?php
/***********************************************************
 * 广告模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
class Ad extends Common
{
    private $ad_position_system_id = array(); //系统默认位置ID，不可删除

    //列表
    public function tableData($post){
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('ad_position')->field($tableWhere['field'])->where($tableWhere['where'])->order($tableWhere['order'])->paginate($limit);
        $data = $this->tableFormat($list->getCollection())->toArray();
        $re['code'] = 0;
        $re['msg'] = '';
        $re['count'] = $list->total();
        $re['data'] = $data;
        return $re;
    }

    //添加/编辑
    public function toAdd($data){
        $result = array('status' => false,'data' => '','msg' => '','url'=>'');
        $domain = Request::baseFile().'/channel.adposition/index';
        //判断是新增还是修改
        if (isset($data['id']) && !empty($data['id'])){
            if(array_key_exists($data['id'], $this->ad_position_system_id)){
                $result['status'] = false;
                $result['msg'] = "不可更改系统预定义位置";
                return $result;
            }
            if(Db::name('ad_position')->where('title','=',$data['title'])->where('id','<>',$data['id'])->count() > 0){
                $result['status'] = false;
                $result['msg'] = "该广告名称已存在，请检查";
                return $result;
            }
            $edit_data = array();
            $edit_data['title'] = $data['title'];
            $edit_data['update_time'] = time();
            if (Db::name('ad_position')->where('id', $data['id'])->data($edit_data)->update()) {
                if(isset($data['img_litpic'])){
                    $i = '1';
                    //读取组合广告位的图片及信息
                    foreach ($data['img_litpic'] as $key => $value) {
                        if (!empty($value)) {
                            // 是否新窗口打开
                            if (!empty($data['img_target'][$key])) {
                                $target = '1';
                            }else{
                                $target = '0';
                            }
                            // 广告位ID，为空则表示添加
                            $ad_id = $data['img_id'][$key];
                            
                            if (!empty($ad_id)) {
                                // 查询更新条件
                                $where = ['id'=> $ad_id];
                                if (Db::name('ad')->where($where)->count() > 0) {
                                    // 主要参数
                                    $AdData['litpic']      = $value;
                                    $AdData['title']       = $data['img_title'][$key];
                                    $AdData['links']       = $data['img_links'][$key];
                                    $AdData['intro']       = $data['intro'][$key];
                                    $AdData['target']      = $target;
                                    // 其他参数
                                    $AdData['sort_order']  = $i++;
                                    $AdData['update_time'] = getTime();
                                    // 更新，不需要同步多语言
                                    Db::name('ad')->where($where)->data($AdData)->update();
                                }else{
                                    // 主要参数
                                    $AdData['litpic']      = $value;
                                    $AdData['pid']         = $data['id'];
                                    $AdData['title']       = $data['img_title'][$key];
                                    $AdData['links']       = $data['img_links'][$key];
                                    $AdData['intro']       = $data['intro'][$key];
                                    $AdData['target']      = $target;
                                    // 其他参数
                                    $AdData['media_type']  = 1;
                                    $AdData['admin_id']    = session::get('admin_id');
                                    $AdData['sort_order']  = $i++;
                                    $AdData['add_time']    = getTime();
                                    $AdData['update_time'] = getTime();
                                    $ad_id = Db::name('ad')->save($AdData);
                                }
                            }else{
                                // 主要参数
                                $AdData['litpic']      = $value;
                                $AdData['pid']         = $data['id'];
                                $AdData['title']       = $data['img_title'][$key];
                                $AdData['links']       = $data['img_links'][$key];
                                $AdData['intro']       = $data['intro'][$key];
                                $AdData['target']      = $target;
                                // 其他参数
                                $AdData['media_type']  = 1;
                                $AdData['admin_id']    = session::get('admin_id');
                                $AdData['sort_order']  = $i++;
                                $AdData['add_time']    = getTime();
                                $AdData['update_time'] = getTime();
                                $ad_id = Db::name('ad')->save($AdData);
                            }
                        }
                    }                    
                }
                $result['msg']    = '修改成功';
                $result['status'] = true;
                $result['url'] = $domain;
                return $result;
            } else {
                $result['msg']    = '修改成功';
                $result['status'] = true;
                $result['url'] = $domain;
                return $result;
            }
        }else {
            //判断用户名是否重复
            $info = Db::name('ad_position')->where('title', $data['title'])->find();
            if ($data['title'] == $info['title']){
                $result['status'] = false;
                $result['msg'] = "该广告名称已存在，请检查";
                return $result;
            }
            //添加广告位置表信息
            $add_data = array(
                'title'       => trim($data['title']),
                'admin_id'    => Session::get('admin_id'),
                'add_time'    => getTime(),
                'update_time' => getTime(),
            );
            $insertId = Db::name('ad_position')->insertGetId($add_data);
            if ($insertId) {
                if(isset($data['img_litpic'])){
                    //读取组合广告位的图片及信息
                    $i = '1';
                    foreach ($data['img_litpic'] as $key => $value) {
                        if (!empty($value)) {
                            if (!empty($data['img_target'][$key])) {
                                $target = '1';
                            }else{
                                $target = '0';
                            }
                            //主要参数
                            $AdData['litpic']      = $value;
                            $AdData['pid']         = $insertId;
                            $AdData['title']       = trim($data['img_title'][$key]);
                            $AdData['links']       = $data['img_links'][$key];
                            $AdData['intro']       = $data['intro'][$key];
                            $AdData['target']      = $target;
                            //其他参数
                            $AdData['media_type']  = 1;
                            $AdData['admin_id']    = Session::get('admin_id');
                            $AdData['sort_order']  = $i++;
                            $AdData['add_time']    = getTime();
                            $AdData['update_time'] = getTime();
                            //添加到广告图表
                            $ad_id = Db::name('ad')->save($AdData);
                        }
                    }                    
                }
                $result['msg']    = '添加成功';
                $result['status'] = true;
                $result['url'] = $domain;
                return $result;
            } else {
                $result['msg']    = '添加失败';
                $result['status'] = false;
                return $result;
            }
        }
    }
    
    protected function pcftableWhere($post){
        $where = [];
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = [];
        return $result;
    }
    
}
