<?php
/***********************************************************
 * 会员配置
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller\users;
use app\admin\controller\Base;
use think\facade\Request;
use think\facade\Db;
use think\facade\Cache;
class Usersconfig extends Base
{

    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = Request::controller().'/index';
        $this->popedom = appfile_popedom($ctl_act);
    }

    // 列表
    public function index(){ 
        if(!$this->popedom["list"]){
            return $this->errorNotice(config('params.auth_msg.list'),true,3,false);
        }
        $users_verification_list1 = ['不验证','后台审核','邮件验证','短信验证'];
        $users_verification_list2 = ['邮件验证','短信验证'];
        $info = $this->getUsersConfig();
        $assign_data = ['users_verification_list1'=>$users_verification_list1,'users_verification_list2'=>$users_verification_list2,'info'=>$info];
        $this->assign($assign_data);
        if (Request::isAjax()) {
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }            
            $post = input('post.');
            if (!empty($post['users'])){
                $this->getUsersConfig($post['users']);
            }
            Cache::clear();//清除数据缓存文件
            $result = ['status' => true, 'msg' => '操作成功'];
            return $result;
        }
        return $this->fetch();
    }
    
    public function getUsersConfig($post=''){
        if($post){
            foreach ($post as $k=>$v){
                $newK = strtolower($k);
                $newArr = array('name'=>$newK,'value'=>trim($v),'update_time'   => time(),);
                if(isset($newK)){
                    Db::name('users_config')->where(['name' => $newK])->save($newArr);
                }
            }
        }else{
            $listdata = array();
            $info = Db::name('users_config')->select();
            foreach($info as $k=>$val){
                $listdata[$val['name']] = $val['value'];
            }
            return $listdata;            
        }
    }

}
