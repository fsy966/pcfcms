<?php
/***********************************************************
 * 会员等级
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller\users;
use app\admin\controller\Base;
use think\facade\Request;
use think\facade\Db;
use app\admin\model\Users_level as level;
class Userslevel extends Base
{

    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = Request::controller().'/index';
        $this->popedom = appfile_popedom($ctl_act);
    }

    // 列表
    public function index(){ 
        if(!$this->popedom["list"]){
            return $this->errorNotice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            $Userslevel = new level();
            return $Userslevel->tableData(input('param.'));
        }
        return $this->fetch();
    }

    //添加
    public function add(){
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        if (Request::isPost()) {
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['code' => 0, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['code' => 0, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            }
            $post = input('param.');
            if(Db::name('users_level')->where('level_name','=',$post['level_name'])->find()){
                $result = ['code' => 0, 'msg' => '已有相同名称！'];
                return $result;
            }
            $add_data = array();
            $add_data['level_name'] = $post['level_name'];
            $add_data['free_day_send'] = isset($post['free_day_send']) ? $post['free_day_send'] : '';
            $add_data['free_all_send'] = isset($post['free_all_send']) ? $post['free_all_send'] : '';
            $add_data['fee_day_top'] = isset($post['fee_day_top']) ? $post['fee_day_top'] : '';
            $add_data['fee_all_top'] = isset($post['fee_all_top']) ? $post['fee_all_top'] : '';
            $add_data['add_time'] = time();
            if (Db::name('users_level')->save($add_data)) {
                $result = ['code' => 1, 'msg' => '添加成功','url'=>Request::baseFile().'/users.userslevel/index'];
                return $result;
            } else {
                $result = ['code' => 0, 'msg' => '添加失败'];
                return $result;
            }  
        }
        return $this->fetch();
    }
    
    //编辑
    public function edit(){
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        if (Request::isPost()) {
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['code' => 0, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['code' => 0, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            $post = input('param.');
            $add_data = array();
            $add_data['id'] = $post['id'];
            $add_data['level_name'] = $post['level_name'];
            $add_data['free_day_send'] = isset($post['free_day_send']) ? $post['free_day_send'] : '';
            $add_data['free_all_send'] = isset($post['free_all_send']) ? $post['free_all_send'] : '';
            $add_data['fee_day_top'] = isset($post['fee_day_top']) ? $post['fee_day_top'] : '';
            $add_data['fee_all_top'] = isset($post['fee_all_top']) ? $post['fee_all_top'] : '';
            $add_data['update_time'] = time();
            if(Db::name('users_level')->save($add_data)){
                $result = ['code' => 1, 'msg' => '修改成功','url'=>Request::baseFile().'/users.userslevel/index'];
                return $result;
            }else{
                $result = ['code' => 0, 'msg' => '修改失败'];
                return $result;
            }
        }
        $info = Db::name('users_level')->where('id',input('param.id/d'))->find();
        if(!$info)$info = "";
        $this->assign('info', $info);
        return $this->fetch();
    }

    // 删除
    public function del(){
         if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $id = input('param.id/d');
            if (Db::name('users_level')->where("id",$id)->delete()) {
                $result = ['status' => true, 'msg' => '删除成功'];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '删除失败'];
                return $result;
            }
            return $result;
        }       
    }

    // 批量删除
    public function batch_del(){
         if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['code' => 0, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['code' => 0, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $id_arr = input('del_id/a');
            $id_arr = eyIntval($id_arr);
            if(is_array($id_arr) && !empty($id_arr)){
                foreach ($id_arr as $key => $val) {
                   Db::name('users_level')->where('id',$val)->delete();
                }
                $result = ['code' => 1, 'msg' => '删除成功！'];
                return $result;
            } else {
                $result = ['code' => 0, 'msg' => '参数有误'];
                return $result;
            }
        }       
    }

}
